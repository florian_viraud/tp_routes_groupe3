from flask import render_template, flash, redirect
from app import app

@app.route('/', methods=('GET',) )
def index():
    return render_template("index.html",
                           title = "Home",
                          )

@app.route('/truc', methods=('GET',) )
def perdu():
    return "Tu t'es perdu", 404

   
@app.route("/mesListes", methods=('GET',))
def mesListes():
    return "Page non implanté", 404

@app.route("/mesListes/supprimer", methods=('GET',))
def supprimerMesListes():
    return "Page non implanté", 404


@app.route("/mesListes/gerer", methods=('GET',))
def gererMesListes():
    return "Page non implanté", 404


@app.route("/mesListes/gerer/ajoutAchat", methods=('GET',))
def ajoutAchatMesListes():
    return "Page non implanté", 404

@app.route('/compte', methods=('GET',) )
def compte():
    return "Page non encore implémentée", 404

@app.route('/deconnexion', methods=('GET',) )
def deconnexion():
    return "Page non encore implémentée", 404


@app.route('/MesCategories', methods=('GET',) )
def categories():
    return "Page non encore implémentée", 404

@app.route('/MesCategories/Add', methods=('GET','POST') )
def categories_Add():
    return "Page non encore implémentée", 404


@app.route('/MesCategories/Remove', methods=('GET','POST') )
def categories_Add():
    return "Page non encore implémentée", 404


@app.route('/Planning/Edit', methods=('GET','POST') )
def planning_edit():
    return "Page non encore implémentée", 404
